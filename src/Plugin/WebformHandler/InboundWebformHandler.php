<?php

namespace Drupal\openinbound\Plugin\WebformHandler;

use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\openinbound\Controller\OI;
use Drupal\Core\Url;

/**
 * Webform submission to OpenInbound handler.
 *
 * @WebformHandler(
 *   id = "openinbound_webform",
 *   label = @Translation("OpenInbound"),
 *   category = @Translation("OpenInbound"),
 *   description = @Translation("Send data to openinbound."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
class InboundWebformHandler extends WebformHandlerBase {

    /**
     * {@inheritdoc}
     */
    public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {



        // Get an array of the values from the submission.
        $data = $webform_submission->getData();
        $config = \Drupal::service('config.factory')->getEditable('openinbound.settings');
        $oi = new OI($config->get('settings.openinbound_tracking_id'), $config->get('settings.openinbound_api_key'));
        \Drupal::logger('oi_debug_tracker_js')->notice(print_r($_COOKIE,1));
        \Drupal::logger('oi_debug_tracker_js')->notice(print_r($data,1));

        $oi->updateContact($_COOKIE['_oi_contact_id'], $data);


        $sid = $webform_submission->id();
        $webform = $this->getWebform();
        $wid = $webform->id();
        $wlabel = $webform->label();

        $submission_url = Url::fromRoute('entity.webform_submission.canonical', ['webform' => $wid, 'webform_submission' => $sid], ['absolute' => TRUE])->toString();


        $params = [];
        $params['href'] = $submission_url;
        $params['referrer'] = '';
        $params['page_title'] = 'View submission';
        $params['title'] = 'Webform Submission '.$webform->label();;
        $params['event_type'] = 'submission';
        $params['raw'] = json_encode($data);
        $oi->addEvent($_COOKIE['_oi_contact_id'], $params);
    }

}
