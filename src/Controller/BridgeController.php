<?php

namespace Drupal\openinbound\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class BridgeController extends ControllerBase
{
   function bridge(Request $request) {
        $config = $this->config('openinbound.settings');
        $openinbound_tracking_id = $config->get('settings.openinbound_tracking_id');
        $openinbound_api_key = $config->get('settings.openinbound_api_key');
        $oi = new OI($openinbound_tracking_id, $openinbound_api_key);

        $operation = $request->get('operation');
        switch ($operation) {
          case "getcontact":
              $contact = $oi->getContactByUUID($request->get('uuid'));
              return JsonResponse::create($contact);
              break;
          case "pageview":
              $params = [];
              $params['href'] = $request->get('href');
              $params['referrer'] = $request->get('referrer');
              $params['page_title'] = $request->get('page_title');
              $params['title'] = 'Pageview';
              $params['event_type'] = 'pageview';
              $event = $oi->addEvent($request->get('contact_id'), $params);
              return JsonResponse::create($event);
              break;
          case "click":
              $params = [];
              $params['href'] = $request->get('href');
              $params['referrer'] = $request->get('referrer');
              $params['page_title'] = $request->get('page_title');
              $params['title'] = $request->get('title');
              $params['event_type'] = 'click';
              $event = $oi->addEvent($request->get('contact_id'), $params);
              return JsonResponse::create($event);
              break;
          case "submit":
              $params = [];
              $params['href'] = $request->get('href');
              $params['referrer'] = $request->get('referrer');
              $params['page_title'] = $request->get('page_title');
              $params['title'] = $request->get('title');
              $params['event_type'] = 'submit';
              $event = $oi->addEvent($request->get('contact_id'), $params);
              return JsonResponse::create($event);
              break;

        }

    }
}
